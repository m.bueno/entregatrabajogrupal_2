import java.util.LinkedList;

public class Administradores {
    private int idAdm;
    private String nombre,
            telefono;
    LinkedList<Edificios> edificios = new LinkedList<Edificios>();

    public int getIdAdm() {
        return idAdm;
    }

    public void setIdAdm(int idAdm) {
        this.idAdm = idAdm;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public LinkedList<Edificios> getEdificios() {
        return edificios;
    }

    public void setEdificios(LinkedList<Edificios> edificios) {
        this.edificios = edificios;
    }

    public Administradores(int idAdm, String nombre, String telefono, LinkedList<Edificios> edificios) {
        this.idAdm = idAdm;
        this.nombre = nombre;
        this.telefono = telefono;
        this.edificios = edificios;
    }

    @Override
    public String toString() {
        return "Administrador: " +
                nombre +
                ", administra los siguientes edificios:\n" + getEdificios();
    }
}