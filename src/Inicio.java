import java.util.LinkedList;
import java.util.List;

public class Inicio {
    public static void main(String[] args) {
        Edificios edificio1 = new Edificios("1", "Edificio 1", "Canelones 983", (byte) 20, (byte) 3);
        Edificios edificio2 = new Edificios("2", "Edificio 2", "Maldonado 983", (byte) 10, (byte) 3);
        Edificios edificio3 = new Edificios("3", "Edificio 3", "Yaguaron 983", (byte) 8, (byte) 4);
        Edificios edificio4 = new Edificios("4", "Edificio 4", "Yi 983", (byte) 15, (byte) 3);
        Edificios edificio5 = new Edificios("5", "Edificio 5", "Rivera 983", (byte) 4, (byte) 2);
        Edificios edificio6 = new Edificios("6", "Edificio 6", "Venezuela 983", (byte) 10, (byte) 3);
        Edificios edificio7 = new Edificios("7", "Edificio 7", "algo 983", (byte) 12, (byte) 5);
        Edificios edificio8 = new Edificios("8", "Edificio 8", "Velazques 983", (byte) 20, (byte) 3);

        LinkedList<Edificios> serieEdificios1 = new LinkedList<Edificios>();
        serieEdificios1.add(edificio1);
        serieEdificios1.add(edificio2);
        serieEdificios1.add(edificio3);
        serieEdificios1.add(edificio4);
        serieEdificios1.add(edificio5);

        LinkedList<Edificios> serieEdificios2 = new LinkedList<Edificios>();
        serieEdificios2.add(edificio6);
        serieEdificios2.add(edificio7);
        serieEdificios2.add(edificio8);

        Administradores administrador1 = new Administradores(1, "Pepe", "234443243", serieEdificios1);
        Administradores administrador2 = new Administradores(2, "Pocho", "344343433", serieEdificios2);

        LinkedList<Administradores> administradores = new LinkedList<Administradores>();
        administradores.add(administrador1);
        administradores.add(administrador2);

        mostrarDatos(administradores);
    }

    public static void mostrarDatos(LinkedList<Administradores> administradores){
        for (Administradores administrador:administradores){
            System.out.println(administrador + "\n");
        }
    }
}
