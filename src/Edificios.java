public class Edificios {
    private String idEdificio,
            nombre,
            direccion;
    private byte cantDePisos,
            cantDeAptosPorPiso;
    Administradores administrador;

    public String getIdEdificio() {
        return idEdificio;
    }

    public void setIdEdificio(String idEdificio) {
        this.idEdificio = idEdificio;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public byte getCantDePisos() {
        return cantDePisos;
    }

    public void setCantDePisos(byte cantDePisos) {
        this.cantDePisos = cantDePisos;
    }

    public byte getCantDeAptosPorPiso() {
        return cantDeAptosPorPiso;
    }

    public void setCantDeAptosPorPiso(byte cantDeAptosPorPiso) {
        this.cantDeAptosPorPiso = cantDeAptosPorPiso;
    }

    public Administradores getAdministrador() {
        return administrador;
    }

    public void setAdministrador(Administradores administrador) {
        this.administrador = administrador;
    }

    public Edificios(String idEdificio, String nombre, String direccion, byte cantDePisos, byte cantDeAptosPorPiso) {
        this.idEdificio = idEdificio;
        this.nombre = nombre;
        this.direccion = direccion;
        this.cantDePisos = cantDePisos;
        this.cantDeAptosPorPiso = cantDeAptosPorPiso;
    }

    @Override
    public String toString() {
        return "\nEdificio id:" + idEdificio + '\'' +
                ", nombre='" + nombre + '\'' +
                ", direccion='" + direccion + '\'' +
                ", cantDePisos=" + cantDePisos +
                ", cantDeAptosPorPiso=" + cantDeAptosPorPiso;
    }
}
